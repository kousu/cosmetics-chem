#!/usr/bin/env python3
"""
A more usable parallel map.
"""

import multiprocessing

def pmap(f, data, *args, **kwargs):
    """
    Perform a parallel map, using multiprocessing.Pool.

    Returns an iterable.

    Caveats:
    - multiprocessing.Pool is *not actually a fully lazy iterator* https://bugs.python.org/issue40110;
     It returns an iterable so you can start processing output as soon as it has input,
     without waiting on the entire dataset, but if your consumer falls behind it will silently buffer,
     potentially eating up large amounts of memory because there's no backpressure mechanism in multiprocessing.Pool.
    - the function passed has to be defined at the top-level of your module; it can't be a lambda or a inner function.

    *args and **kwargs are passed through to multiprocessing.Pool()'s constructor, so e.g.

    # XXX this is not doctestable because of the second caveat :/
    #>>> p = lambda x: x**2
    #>>> list(pmap(p, range(17), 32))
    #[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256]

    Will run on 32 processes (so ideally, 32 cores, if your kernel is friendly and your system unloaded).

    This is an improvement over using multiprocessing.Pool().imap() because the lifetime
    of the pool is as long as its needed. For example, you can write:

    def pipeline(data):
        data = pmap(step1, data)
        data = pmap(step2, data)
        return data

    with imap() you would have to write

    def pipeline(data):
        with multiprocessing.Pool() as pool:
            data = pool.imap(step1, data)
            data = pool.imap(step2, data)
            return data

    but this deadlocks, because the pool can't close until it's processed all its data,
    and it can't process all its data until it returns and the caller consumes it.

    You could fall back on

    def pipeline(data):
        with multiprocessing.Pool() as pool:
            data = pool.map(step1, data)
            data = pool.map(step2, data)
            return data

    but that is unlazy and could easily block downstream processing for a long time.
    In fact, the first thing Pool.map() does is call list() on data, meaning it
    *first* blocks waiting for upstream to produce its entire dataset.

    This is named pmap() and not pimap() because map() is already lazy in python3,
    so no need to have the extra.

    Compare: Julia: https://docs.julialang.org/en/v1/stdlib/Distributed/#Distributed.pmap.
    """

    # TODO: if I do rewrite multiprocessing.Pool to have better ergonomics,
    # is it possible to inline this over there?

    with multiprocessing.Pool(*args, **kwargs) as pool:
        yield from pool.imap(f, data)

if __name__ == '__main__':
    import sys
    import time
    import random
    def step1(x):
        time.sleep(random.randint(3,8)/100) # simulate work
        print(("step1(%d)" % (x,)), file=sys.stderr)
        return x**3

    def step2(y):
        time.sleep(random.randint(8, 17)/100) # simulate work
        print(("step2(%d)" % (y,)), file=sys.stderr)
        return 4*y**3 + 99*y - 8

    def pipeline(input):
        """
        >>> pipeline([1,2,3])
        [95, 2832, 81397]

        >>> pipeline(range(50))
        [-8, 95, 2832, 81397, 1054904, 7824867, 40332160, 161448377, 536921592, 1549754119, 4000098992, 9431922525, 20639292472, 42418214987, 82644458784, 153773771617, 274878312440, 474351992367, 793437738832, 1290751470149, 2048000791992, 3177121103155, 4829077925312, 7204611850377, 10567231529464, 15258790609367, 21718016455920, 30502391888557, 42313825986872, 58028586317979, 78732002672992, 105758491591985, 140737491599352, 185645941165567, 242867974956944, 315262558932117, 406239831292600, 519846964194947, 660864410483712, 834913450507609, 1048576006335992, 1309527744399015, 1626685542732592, 2010370455618557, 2472487366471224, 3026722579333867, 3688760660312480, 4476521902689537, 5410421853327352, 6513654403289039]
        """

        # okay now to rewrite this as a pipeline....
        # what's the best way to indent this?
        # I want to do something like https://dbader.org/blog/python-iterator-chains
        # or https://brett.is/writing/about/generator-pipelines-in-python/
        # the former recommends writing chains as
        # step1 = process1(input)
        # step2 = process2(step1)
        # ...
        # output = processN(stepN_1)
        #
        # though you could also write
        # output = processN(processN_1(..... (process2(process1(input)))
        # but the former version, even though more verbose,
        # matches how we read
        #

        #pool = multiprocessing.Pool(16) # XXX the canonical way to use this is in a `with`:
          # by default Pool uses as many workers as CPUs
          # but, at least on my Chromebook, experimentally, it's actually faster to add more workers than CPUs
          # (to a point; above ~32 the overhead in forking the workers starts to overtakes the boost)

        print("Defining v1", file=sys.stderr)
        v1 = pmap(step1, input, 1)
        print("Defining v2", file=sys.stderr)
        v2 = pmap(step2, v1, 100)
        print("Done", file=sys.stderr)
        output = v2
        print("Computing", file=sys.stderr)

        #pool.close()
        #pool.join()

        return list(output)

    import doctest
    if doctest.testmod().failed:
        raise SystemExit(1)
