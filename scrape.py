#!/usr/bin/env python3
"""
Scrape the Environmental Working Group (EWG) [database](https://www.ewg.org/skindeep/) of cosmetic products.

This contains products, their ingredients, and associated health/environmental concerns.
As a side effect, it also contains a fairly comprehensive synonym-set of chemical names, which is pretty useful just on its own.
"""

import os.path
import re

import requests_cache
requests_cache.install_cache(".ewg-pages") # This *must* be before importing requests_html:
                                           # https://github.com/reclosedev/requests-cache/issues/149
from requests_html import HTMLSession # pip install requests-html
#import requests.exceptions

def unjoin(s: str):
    """
    Undo the EWG list-to-text algorithm.

    The algorithm their webpage uses to turn list data into text
    seems to be to join everything with ", " but replacing the *last*
    with ", and "
    This undoes that, recovering the origin list of texts.

    # Beware: this is case-sensitive.
    # Bugs: breaks on "Use restrictions (moderate), Irritation (skin, eyes, or lungs) (moderate), and Contamination concerns (high)"

    >>> unjoin("CAUSTIC POTASH, CAUSTIC POTASH; POTASSIUM HYDROXIDE; POTASSIUM HYDROXIDE, POTASSIUM HYDROXIDE, and POTASSIUM HYDROXIDE (K(OH))")
    ['CAUSTIC POTASH', 'CAUSTIC POTASH; POTASSIUM HYDROXIDE; POTASSIUM HYDROXIDE', 'POTASSIUM HYDROXIDE', 'POTASSIUM HYDROXIDE (K(OH))']

    >>> unjoin("a, b, c")
    ['a', 'b', 'c']

    >>> unjoin("a, b, and c")
    ['a', 'b', 'c']

    >>> unjoin("a, b and c")
    ['a', 'b', 'c']

    >>> unjoin("a, b , c")
    ['a', 'b ', 'c']

    # XXX this isn't 100% correct, e.g. this will do something sensible
    # though it will choke on:
    >>> unjoin("a, and b, c")  ## what should this do??
    ['a', 'b', 'c']

    # Though the *real* inverse function would give:
    # because it should only treat an 'and' as a delimiter if it occurs *after* all ","s.
    #>>> unjoin("a, and b, c")
    #['a', 'and b', 'c']

    # And also it can't handle text with embedded commas (or ands),
    # which unfortunately show up in the real data:
    >>> unjoin("Use restrictions (moderate), Irritation (skin, eyes, or lungs) (moderate), and Contamination concerns (high)")
    ['Use restrictions (moderate)', 'Irritation (skin, eyes, or lungs) (moderate)', 'Contamination concerns (high)']
    """

    # Replace the final (but optional) special-case  ", and " or " and " with a single delimiter.
    # This relies on a little trick: since " and " is a substring of ", and ", we do the latter first
    # to make sure that

    # stupid hack: because this is confused by substrings with embedded commas (obviously)
    # hide a manual list of "stopwords" that we found in our data, map them to unprintable gunk
    # to temporarily put them off to the side while we deal with the main algorithm
    # this is basically the mime delimiter hack from email. Hoorah :)
    # TODO: the better long term fix is something like...make sure to ignore commas if they appear within parentheses?
    bad_strings = {
      "(skin, eyes, or lungs)": "\x02\x03\x03",
      # ...
      # TODO: https://www.ewg.org/skindeep/ingredients/700215-ALCOHOL_DENATURED/ has some tricky edge cases
    }
    for k,v in bad_strings.items():
        s = s.replace(k,v)

    # https://stackoverflow.com/questions/9943504/right-to-left-string-replace-in-python/9943875#9943875
    s = ', '.join(s.rsplit(', and ', maxsplit=1))
    s = ', '.join(s.rsplit(' and ', maxsplit=1))

    r = s.split(", ")

    # stupid hack part 2: undo the mapping
    for v,k in bad_strings.items():
        r = [e.replace(k, v) for e in r]

    return r

def url_ID(url):
    """
    Extract a numeric article ID from a SEO-friendly URL, the kind that is site.com/path/to/section/134234-long-article-slug.
    The long-article-slug part is almost never a

    >>> url_ID("https://www.ewg.org/skindeep/ingredients/702196-2,4-IMIDAZOLIDINEDIONE/")
    702196

    >>> url_ID("https://www.ewg.org/skindeep/browse/ingredients/702196")
    702196

    >>> url_ID("https://www.ewg.org/skindeep/browse/ingredients/702196DMDM_HYDANTOIN_FORMALDEHYDE_RELEASER/")
    702196

    >>> url_ID("https://www.theatlantic.com/health/archive/2020/03/how-will-coronavirus-end/608719/")
    608719

    # TODO: think about how to handle this case. Return a string afterall? A different function?
    #>>> url_ID("https://medium.com/@kaistinchcombe/decentralized-and-trustless-crypto-paradise-is-actually-a-medieval-hellhole-c1ca122efdec")
    #0xc1ca122efdec
    """
    # find the longest numeric substring in the last (nonempty) path segment
    return int(
            re.match(
              r'.*?(\d+).*?', # NB: greedy match surrounded by two ungreedy matches
              str(url).rstrip('/').split('/')[-1])
            .groups()[0])


def extract_ingredient(page):
    """
    Extract the interesting data from https://www.ewg.org/skindeep/ingredients/$ID-*/
    """
    type = 'ingredient'
    ewg_id = url_ID(page.find('link[rel=canonical]', first=True).attrs['href'])
    main = page.find('main', first=True)

    pubchem_id = main.find('#chemical > .chemical-image > p > a', first=True)
    if pubchem_id is not None:
        # not all ingredients have a pubchem ID, especially the hippy ones
        # e.g. https://www.ewg.org/skindeep/ingredients/862388-SEQUOIA_SEMPERVIRENS_LEAF_CELL_EXTRACT/
        pubchem_id = url_ID(pubchem_id)

    # this is the paydirt:
    # in Wordnet these are called synsets (see https://wordnet.princeton.edu/):
    # > Synonyms--words that denote the same concept and are interchangeable in many contexts--are grouped into unordered sets (synsets)
    # i.e. the set of words for a concept set effectively *is* the concept
    # for words it's there's fuzzy meanings, connotations, and collisions, but luckily chemicals are more strictly named
    # so hopefully this will be one concept to many names, uniquely, and we can map backwards.
    chemsyn = main.find(".chemical-synonyms-text", first=True)
    chemsyn = unjoin(chemsyn.text)

    functions = main.find('.chemical-functions-text', first=True)
    functions = unjoin(functions.text)

    concerns = main.find('.chemical-concerns-text', first=True)
    concerns = unjoin(concerns.text)

    about = main.find('.chemical-about-text', first=True).text

    del page, main
    return locals() # ha ha ha silly python trix are for kids


def extract_product(page):
    type = 'product'
    ewg_id = url_ID(page.find('link[rel=canonical]', first=True).attrs['href'])
    main = page.find('main', first=True)

    name = main.find('h2.product-name', first=True).text

    # TODO: other things we might want:
    # - picture?
    # - score? -> kind of annoying to extract as its only provided as an image; but we could extract it from the image URL.
    # - PETA score
    # - original label text

    ingredients = [list(a.absolute_links)[0] for a in main.find('.td-ingredient-interior > a')]

    del page, main
    return locals()


def extract_categories(page):
    """
    Extract the list of product categories from the nav menu on the EWG database.
    """

    product_categories = page.html.find('nav', first=True).absolute_links
    product_categories = [e for e in product_categories if "/skindeep/browse/category" in e]
    # now let's do some normalization
    # assumption 1: query strings are redundant (e.g. ?marketed_for=men); just removing them will get a larger category
    # assumption 2: "%20", " " and "_" are interchangable
    # assumption 3: there are redundant links to the same categories, even not counting the normaliz
    def norm_cat_url(url):
        url = url.split("?", maxsplit=1)[0] # should I use urllib.parse here?
        url = url.replace("%20", " ").replace(" ", "_")
        return url
    product_categories = [norm_cat_url(e) for e in product_categories]
    product_categories = list(set(product_categories)) # remove duplicates
    product_categories = sorted(product_categories)
    return product_categories


def extract_category(page):
    """
    Get the list of product pages within a category page.

    """

    #products = page.find('section.product-listings', first=True) # this is fucking buggy https://github.com/psf/requests-html/issues/295
    products = page
    products = products.absolute_links   # we can probably just brute-force it instead of trying to zero in by CSS
    products = sorted([e for e in products if "/skindeep/products/" in e])

    return products



def scrape_category(args):
    i, category = args
    print("%03d/%03d" % (i, -1), category)
    category = session.get(category+"?per_page=10000") # hack: to avoid a nested loop plus an extra layer of unioning, just *ask the server to give the whole category*. hopefully 10000 is larger than any category.
    # (actually an extra layer of unioning wouldn't be terrible; but this is probably faster anyway)
    assert category.html.find('a[rel="next"]', first=True) is None, "Got a nav link on %s, is the category too large??" % (category.url,)
    return extract_category(category.html)

def scrape_skindeep():
    """
    Main entrypoint for scraping the EWG website.

    The output is a sqlite database with these tables:
    - products: (product_id, name, manufacturer, ...)
    - ingredients: (ingredient_id, name, concerns, data, chemsyn, ....)
    - product_ingredients: (product_id, ingredient_id)
    """

    # The EWG website is laid out to highlight uses by humans, not to show off the underlying SQL.
    # It only has partial (per category) indexes for products and has no index at all of ingredients.
    # Because of all that, we have to get the data out by following the breadcrumbs:
    # 0. https://www.ewg.org/skindeep/ to get categories
    # 1. https://www.ewg.org/skindeep/browse/category/* to get products
    # 2. https://www.ewg.org/skindeep/products/* to get product details, plus ingredients
    # 3. https://www.ewg.org/skindeep/ingredients/* to get ingredient details
    # TODO: also extract https://www.ewg.org/skindeep/browse/brands/*


    global session # XXX beware
    session = HTMLSession()

    # inline raise_for_status() on every get()
    session.hooks["response"].append(lambda r, *args, **kwargs: r.raise_for_status())

    # EWG is behind CloudFlare, whose DDoS protection is pretty easy to trigger;
    #   this handles its HTTP 429s transparently.
    # https://findwork.dev/blog/advanced-usage-python-requests-timeouts-retries-hooks/#retry-on-failure
    from requests.adapters import HTTPAdapter
    from requests.packages.urllib3.util.retry import Retry
    session.mount("https://",
                  HTTPAdapter(max_retries=Retry(total=3,
                                                backoff_factor=5)))

    #import http; http.client.HTTPConnection.debuglevel = 1 # DEBUG

    index = session.get('https://www.ewg.org/skindeep/')
    # there doesn't seem to be a single master index page to all products, nor to all ingredients
    # so we need to scrape each category index individually, merge them, and then use the products
    # retrieved from that to spider the ingredient pages.
    categories = extract_categories(index)
    assert len(categories) == 95 # spot-check; would need updating as EWG evolves.
    categories = sorted(categories)
    categories = categories[:3]

    # ..is parallelizing the downloads going to be a problem?
    # will that corrupt my database?
    # right now I have something like
    # <<<"https://ewg.org/skindeep" | main | categories | products | ingredients
    # each passing the URLs of the next targets along (and with some side outputs going to the cache)
    # the safe fix would be to add extra inner stages:
    # <<<"https://ewg.org/skindeep" | download | parallel parse_main | download | parallel parse_categories | download | parallel parse_products | download | parallel parse_ingredients

    # keeping the download steps up here in the main process (you wouldn't want to
    # and if we did that it would probably be good to do https://requests-html.kennethreitz.org/#using-without-requests
    # so that we could split the HTML parsing into the parsing part and away from the downloading part entirely
    # since, with the cache in place, that really seems to be the slowest part??
    # another issue: if the downloading only happens in the main thread
    # this would all be
    # ...but if we do this
    # ack
    # I don't knowwwwwwwwwwww

    import multiprocessing
    pool = multiprocessing.Pool(4)

    import time

    t0 = time.time()

    #products = set()
    #for i, category in enumerate(categories, 1):
    #    print("%03d/%03d" % (i, len(categories)), category)
    #    category = session.get(category+"?per_page=10000") # hack: to avoid a nested loop plus an extra layer of unioning, just *ask the server to give the whole category*. hopefully 10000 is larger than any category.
    #    # (actually an extra layer of unioning wouldn't be terrible; but this is probably faster anyway)
    #    assert category.html.find('a[rel="next"]', first=True) is None, "Got a nav link on %s, is the category too large??" % (category.url,)
    #    products.update(extract_category(category.html)) # <-- returns a list of products\
    products = [p for products in pool.map(scrape_category, enumerate(categories)) for p in products]
    products = sorted(products, key=url_ID) # stabilize output; XXX this is unnecessary and can be skipped

    print("Took %.02fs to download categories" % (time.time() - t0,))
    #raise SystemExit


    from pprint import pprint
    #pprint(sorted(products))
    products = products[:4]
    print("Found %d products" % len(products))

    # 2. https://www.ewg.org/skindeep/products/* to get product details, plus ingredients
    product_details = [] # ???
    ingredients = set()
    for i, product in enumerate(products, 1):
        print("%06d/%06d" % (i, len(products)), product, "(%d ingredients found)" % len(ingredients))

        product = session.get(product)
        if product.html.find('main', first=True).search("ERROR CODE: 404"):
            # there are broken links in the database
            # which of course aren't returned as actual 404s.
            continue
        product = extract_product(product.html)
        product_details.append(product)

        ingredients.update(product['ingredients'])
    ingredients = sorted(ingredients, key=url_ID) # ditto; XXX also not strictly necessary
    ingredients = ingredients[:4]
    print("Found %d ingredients" % len(ingredients))

    # 3. https://www.ewg.org/skindeep/ingredients/* to get ingredient details
    ingredient_details = []
    for i, ingredient in enumerate(ingredients, 1):
        print("%06d/%06d" % (i, len(ingredients)), ingredient)

        page = session.get(ingredient).html
        if page.find('main > .browse-search-header', first=True):
            # the EWG site doesn't report missing ingredients/products as 404s,
            # it just says "search failed" and gives you a search box; so help it out
            #raise requests.exceptions.HTTPError("404")

            # ack, but the EWG database has some broken links in it?
            # we can't do much about that so... skipped!
            continue

        ingredient_details.append(extract_ingredient(page))

    # 4. Reformat as SQL.

    # 4a. Extract the many-to-many join table between products and ingredients
    # (by unioning up all the entries)
    product_ingredients = set((product['ewg_id'], url_ID(ingredient))
                               for product in product_details
                               for ingredient in product['ingredients'])
    pprint(product_ingredients)

   # 4b. Extract a 1-1 ewg_id <-> pubchem_id join table
   # ...
   # 4c. Extract a 1-to-many chemical synonyms join table
   # ...
   # 4d. Extract a many-to-many concerns join table
   # ...


    # TODO: can this be written as a nice series of chained maps? as a pipeline?
    # TODO: we should probably write directly to SQLite instead of buffering in python

    # TODO: also extract the concerns and functions tables (right now they just exist as lists on each ingredient, but could/should be turned into many-to-many tables)


def test_extract_ingredient():
    session = HTMLSession()

    r = session.get('https://www.ewg.org/skindeep/ingredients/702196-DMDM_HYDANTOIN/')
    r = extract_ingredient(r.html)

    from pprint import pprint
    pprint(r)
    # XXX make this an actual unit test?


# TODO: make this test more reliable/faster (eg by putting the things that need doctesting in a util.py module or something)
import doctest
if doctest.testmod().failed:
    raise SystemExit(1)

if __name__ == '__main__':
    scrape_skindeep()
